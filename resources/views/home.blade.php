@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           <form action="/posts/create" method="post">
            @csrf
                <div class="form-group">
                    <label for="title" class="control-label">Title</label>
                    <input type="text" name="title" id="title" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">SAVE</button>
           </form>
        </div>
    </div>
</div>
@endsection
